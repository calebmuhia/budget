from tastypie.authentication import Authentication, BasicAuthentication, ApiKeyAuthentication, MultiAuthentication, SessionAuthentication
from tastypie.authorization import Authorization, DjangoAuthorization
from tastypie.exceptions import BadRequest
from tastypie import fields


from django.conf.urls import *
from django.core.paginator import Paginator, InvalidPage
from django.http import Http404
#from haystack.query import SearchQuerySet
from tastypie.resources import ModelResource, Resource
from tastypie.utils import trailing_slash


from tastypie.constants import ALL, ALL_WITH_RELATIONS

import logging 
logger = logging.getLogger(__name__)

from core.models import Budget, BudgetEstimate, Transaction
from categories.models import Category
from datetime import datetime, timedelta
from django.db.models import Q

from django.conf.urls import *


def _get_parameter(request, name):
    '''
    Helper: abstraction to recover paremeters from POST or GET
    either
    '''

    if request.POST:
        try:
            return request.POST[name]
        except KeyError:
            return None
    if request.GET:
        try:
            return request.GET[name]
        except KeyError as e:
            return None

class CategoryResource(ModelResource):

    children = fields.ToManyField('self', 'children', full=True, blank=True, null=True)

    class Meta:
        queryset = Category.objects.all()
        list_allowed_methods = ['get','post','put' ]
        detail_allowed_methods = ['get','post','put','delete' ]

        resource_name = 'category'
        limit=0
        allowed_methods = ['get', 'post', 'put']
        authentication = Authentication()
        authorization = Authorization() # THIS IS IMPORTANT

        always_return_data = True
        filtering = {
        "level":ALL
        }


class BudgetEstimateResource(ModelResource):
    budget_category = fields.ForeignKey(CategoryResource, 'budget_category', null=True, blank=True,full=True )
    budget          = fields.ForeignKey("core.api.BudgetResource", "budget")

    class Meta:
        queryset = BudgetEstimate.objects.all()
        list_allowed_methods = ['get','post','put' ]
        detail_allowed_methods = ['get','post','put','delete' ]
        authentication = Authentication()
        authorization = Authorization() # THIS IS IMPORTANT

        resource_name = 'budget_estimate'
        limit=0
        
        always_return_data = True

    def prepend_urls(self):
        return[
            url(r"^(?P<resource_name>%s)/get_recurring%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('get_recurring'), name="api_get_recurring"),
        ]

    def get_recurring(self, request, **kwargs):

        self.method_check(request, allowed=['get'])
        self.is_authenticated(request)
        self.throttle_check(request)

        date = _get_parameter(request, "date")
        current_budget = _get_parameter(request, 'current_budget')

        print date
         
        
        
        try:
            current_budget = Budget.objects.get(id = current_budget)

            budget = Budget.objects.get(start_date = date)

            budget_estimates = BudgetEstimate.objects.filter(budget = budget)

            for budget_estimate in budget_estimates:
                print BudgetEstimate.objects.filter(Q(budget=current_budget) & Q(budget_category = budget_estimate.budget_category)).exists()

                if BudgetEstimate.objects.filter(Q(budget=current_budget) & Q(budget_category = budget_estimate.budget_category)).exists():
                    print "exists"
                    continue
                
                BudgetEstimate.objects.create(
                    budget = current_budget,
                    budget_category = budget_estimate.budget_category,
                    amount = budget_estimate.amount
                    )
        except Exception, e:
            print e
            pass

        objects = {}   

        self.log_throttled_access(request)
        return self.create_response(request, objects)    







        

class BudgetResource(ModelResource):

    budgetestimate = fields.ToManyField(BudgetEstimateResource, "budget", null=True, blank=True,full=True)


    class Meta:
        queryset = Budget.objects.all()
        list_allowed_methods = ['get','post','put' ]
        detail_allowed_methods = ['get','post','put','delete' ]

        resource_name = 'budget'
        limit=0

        filtering = {
        'start_date':ALL
        }
       
        always_return_data = True

    def obj_get_list(self, bundle, **kwargs):
        if bundle.request.method == 'GET':
            try:
                start_date = bundle.request.GET['start_date']
            
                obj, created = Budget.objects.get_or_create(start_date = start_date)
            except:
                pass    

        objects = ModelResource.obj_get_list(self, bundle, **kwargs)
        return objects







class TransactionResource(ModelResource):
    category = fields.ForeignKey(CategoryResource, 'category', null=True, blank=True,full=True )
    sub_category = fields.ForeignKey(CategoryResource, 'sub_category', null=True, blank=True,full=True )


    class Meta:
        queryset = Transaction.objects.all().order_by("-created")
        list_allowed_methods = ['get','post','put' ]
        detail_allowed_methods = ['get','post','put','delete' ]
        authentication = Authentication()
        authorization = Authorization() # THIS IS IMPORTANT

        resource_name = 'transaction'
        limit=0

        filtering = {
        'date1':ALL,
        'date2':ALL,
        }

        
        always_return_data = True

    def build_filters(self, filters=None):
        """
        tag is not a proper field attr for image, to filter by tag, we need to 
        create a custom filter  
        """

        if filters is None:
            filters = {}

        orm_filters = super(TransactionResource, self).build_filters(filters)


        if 'date1' in filters and 'date2' in filters:
            date1 = filters['date1']
            date2 = filters['date2']

            today = datetime.strptime(date1, "%Y-%m-%d")
            lastweek = datetime.strptime(date2, "%Y-%m-%d" )
            today = today+timedelta(hours=23)

            qset_date =(Q(date__range=[lastweek,today]))
            orm_filters.update({'dates': qset_date})    

       

        return orm_filters

    def apply_filters(self, request, applicable_filters):
        '''
          apply the custom filter
        '''

        

        if "dates" in applicable_filters:
            dates = applicable_filters.pop('dates')

        else:
            dates = None        

            

        semi_filtered = super(TransactionResource, self).apply_filters(
            request, applicable_filters)
        # print semi_filtered

        if dates:
            semi_filtered = semi_filtered.filter(dates).distinct()


           

        return semi_filtered 


