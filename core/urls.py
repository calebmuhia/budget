from django.conf.urls import patterns, include, url
from core.api import CategoryResource, BudgetResource, BudgetEstimateResource, TransactionResource
from core.views import Index
from tastypie.api import Api
v1_api = Api(api_name='v1')

v1_api.register(CategoryResource())
v1_api.register(BudgetResource())
v1_api.register(BudgetEstimateResource())
v1_api.register(TransactionResource())



urlpatterns = patterns('',
       url(r'^$', Index.as_view(), name="home"),  

       url(r'^api/', include(v1_api.urls)),  

)