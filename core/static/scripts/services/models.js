'use strict'


var budgetApp = angular.module("budgetApp")


angular.forEach([
    "Category",
    "Budget",
    "Budget_estimate",
    "Transaction"], function(val, index){
        budgetApp.factory(val, ["$resource", "$rootScope", function($resource, $rootScope){

            return $resource('/api/v1/'+camelToDash(val)+'/:id/:extra_path/',
                { id: '@id' },
                {
                    update: { method: 'PUT' },
                    get: {isArray: false, method: 'GET'},
                    query: {method:'GET', params:{}},
                    delete: { method: 'DELETE' },
                    post: { method: 'POST' }
                });





        }])
    })
