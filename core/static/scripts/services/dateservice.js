'use strict'

angular.module("budgetApp")

.factory('DateService', ['$rootScope','$filter', function($rootScope, $filter){
    
   var DateService = function(){
    this.date1 =  new Date();
    this.date2 = new Date();
    this.date2.setDate(this.date1.getDate()-7)
    this.budgetdate = new Date()
    this.budgetdate = new Date(this.budgetdate.getFullYear(),  this.budgetdate.getMonth(), 1)


   }

  DateService.prototype.date1_toString = function() {
      var date1 = $filter('date')(this.date1, 'yyyy-MM-dd')
      return date1

  };

  DateService.prototype.date2_toString = function() {
      var date2 = $filter('date')(this.date2, 'yyyy-MM-dd')
      return date2
  };

   DateService.prototype.budgetdate_toString = function() {
      var budgetdate = $filter('date')(this.budgetdate, 'yyyy-MM-dd')
      return budgetdate
  };

  


  DateService.prototype.submit_dates = function() {
      $rootScope.$emit('submit')
  };

  DateService.prototype.submit_budget_dates = function() {
      $rootScope.$emit('submit_budget_dates')
  };


   return new DateService

}])