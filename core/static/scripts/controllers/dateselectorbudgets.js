
'use strict'


angular.module('budgetApp')

.controller('BudgetDateSelectorCtrl', ['$scope','$rootScope','$filter','DateService', function($scope,$rootScope,$filter,DateService){

    $scope.budgetdate = DateService.budgetdate

    console.log($scope.budgetdate)

    $scope.next_month =function(){

       $scope.budgetdate = new Date($scope.budgetdate.getFullYear(),  $scope.budgetdate.getMonth()+1, 1)
        
    }

    $scope.previous_month =function(){
       $scope.budgetdate = new Date($scope.budgetdate.getFullYear(),  $scope.budgetdate.getMonth()-1, 1)
        
    }

    $scope.open_date1 = function($event) {
    $event.preventDefault();
    $event.stopPropagation();

    $scope.date1_opened = true;
    
      };

   $scope.$watch("budgetdate", function(month){
    $scope.submit()
   })


  $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];

  $scope.submit = function(){
    DateService.budgetdate = $scope.budgetdate
    

    DateService.submit_budget_dates()

  }

    
}])