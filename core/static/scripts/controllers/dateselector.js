'use strict'


angular.module('budgetApp')

.controller('DateSelectorCtrl', ['$scope','$rootScope','$filter','DateService', function($scope,$rootScope,$filter,DateService){

    $scope.date1 = DateService.date1
    $scope.date2 = DateService.date2

    $scope.open_date1 = function($event) {
    $event.preventDefault();
    $event.stopPropagation();

    $scope.date1_opened = true;
    $scope.date2_opened = false;
      };

    $scope.open_date2 = function($event) {
    $event.preventDefault();
    $event.stopPropagation();

    $scope.date2_opened = true;
    $scope.date1_opened = false;

  };

  $scope.dateOptions = {
    formatYear: 'yy',
    startingDay: 1
  };

  $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];

  $scope.submit = function(){
    DateService.date1 = $scope.date1
    DateService.date2 = $scope.date2

    DateService.submit_dates()

  }

    
}])

