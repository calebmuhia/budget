'use strict'

angular.module("budgetApp")

.controller('TransactionsCtrl', ['$scope','Category','Transaction','$rootScope','DateService', '$http','$modal',"$cookies", function($scope,Category,Transaction, $rootScope,DateService, $http, $modal, $cookies){

 
 $scope.get_data = function(){

    var date1 = DateService.date1_toString();
    var date2 = DateService.date2_toString();

    $scope.transactions_promise = Transaction.get({date1:date1,date2:date2,limit:30}).$promise
    .then(function(response){
        $scope.transactions = response.objects;
        $scope.meta = response.meta;
        console.log($scope.meta)

    })
 }

 $scope.get_data()

 $scope.get_previous = function(){
   var previous_url = $scope.meta.previous;

    if (previous_url !=undefined){
        $scope.transactions_promise = $http.get(previous_url)
        .then(function(response){
            $scope.transactions = response.data.objects;
            $scope.meta = response.data.meta;

        });
    

    };
    

 };

 $scope.get_next = function(){
    var next_url = $scope.meta.next;
    if (next_url){
        $scope.transactions_promise = $http.get(next_url)
        .then(function(response){
   
            $scope.transactions = response.data.objects;
            $scope.meta = response.data.meta;
   

        });
    

    };

 };



  

 $rootScope.$on('submit', function($event){
    $scope.get_data()
 })

 $scope.create_transaction = function(){

    var modalInstance = $modal.open({
       templateUrl: 'transaction_form_template.html',
       controller: 'CreateTransaction',
       size: 'lg',
       
     });
 
     modalInstance.result.then(function (transaction) {

        transaction['csrfmiddlewaretoken'] = $cookies['csrftoken'];
       
 $scope.transactions_promise = Transaction.post(transaction).$promise
 .then(function(response){
    $scope.transactions.unshift(response)
 },
   function(error){
    console.log(error)
   }
 )
        



        }, function () {
       console.log('Modal dismissed at: ' + new Date());
     });
   };

}])

.controller('CreateTransaction', ['$scope','Category','$modalInstance', function($scope,Category,$modalInstance){
    $scope.transaction_type = ["expense","income"]
    $scope.new_transaction = {
        amount:0.00,
        transaction_type:'expense',
        category:{},
        sub_category:{},
        notes:''
    }

    Category.get({level:0}).$promise
    .then(function(response){

    $scope.categories = response.objects
    $scope.new_transaction.category = $scope.categories[0]
    $scope.sub_categories = $scope.new_transaction.category.children
    $scope.new_transaction.sub_category = $scope.sub_categories[0]
    })

    $scope.change_category_children = function(category){
        console.log(category)
        $scope.sub_categories = category.children
        $scope.new_transaction.sub_category = $scope.sub_categories[0]

    }

    $scope.ok =  function(){
        $modalInstance.close($scope.new_transaction)
        
    }
    $scope.cancel = function(){
        $modalInstance.dismiss()
    }
    
}])