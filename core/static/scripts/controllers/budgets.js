'use strict'


angular.module('budgetApp')


.controller('BudgetsCtrl', ['$scope','$rootScope',"DateService",'Budget','$modal','Budget_estimate','$cookies','$filter', function($scope,$rootScope,DateService,Budget,$modal,Budget_estimate,$cookies,$filter){

    $scope.budgetdate  = DateService.budgetdate
    
    //a check foar the current month to disallow editing past budget estimates

    $scope.isCurrentMonth = true

    $scope.is_current_month =  function(){
        var month = DateService.budgetdate.getMonth();
        var current_month = new Date().getMonth();
        
        $scope.isCurrentMonth = (current_month==month);


    }//is_current_month()
    
    //get_data()
    $scope.get_data = function(){
        $scope.budgetdate  = DateService.budgetdate_toString()
        $scope.budgets_promise = Budget.get({start_date:$scope.budgetdate}).$promise
        .then(function(response){
            $scope.budget_object = response.objects[0]
            $scope.budget_estimates = $scope.budget_object.budgetestimate
        })   
        $scope.is_current_month() 
    }//end get_data()

    $rootScope.$on("submit_budget_dates", function($event){
        $scope.get_data()
    })
    
    // add a new budget estimate
    $scope.add_budgetEstimate = function(){
        // open the modal instance
        var modalInstance = $modal.open({
          templateUrl: 'Budget_Estimate.html',
          controller: 'BudgetEstimateCtr',
          size: 'lg',
          resolve: {
            //budget_estimate_categories()
            budget_estimates_categories:function(){
                var categories = [];
                angular.forEach($scope.budget_estimates, function(val, indx){
                    categories.push(val.budget_category)
                })
                return categories
            }//budget_estimate_categories()

            
          }
        });// $modal.open()


        // modalIntance()
        modalInstance.result.then(
            //sucess()
            function (budget_estimate) {

            budget_estimate['csrfmiddlewaretoken'] = $cookies['csrftoken'];
            budget_estimate.budget = $scope.budget_object.resource_uri;
            // budget_estimate.budget['csrfmiddlewaretoken'] = $cookies['csrftoken'];

           $scope.budgets_promise= Budget_estimate.post(budget_estimate).$promise
            .then(function(response){
                $scope.get_data()
            })

        },// sucess()
          //error()
         function () {
          console.log('Modal dismissed at: ' + new Date());
        }// error()
        );
      };// end create budget estimate

    //edit_budget_estimate()



    $scope.edit_budget_estimate = function(budget_estimate){

        //modal.open()      
        var modalInstance = $modal.open({
              templateUrl: 'edit_budget_estimate.html',
              controller: 'EditBudgetEstimateCtrl',
              size: 'lg',
              resolve: {
                budget_estimate:function(){
                return budget_estimate
                },
                budget_estimates_categories:function(){
                var categories = [];
                angular.forEach($scope.budget_estimates, function(val, indx){
                    categories.push(val.budget_category)
                })
                return categories
            }//budget_estimate_categories()

              }
            });//modal.open()
            //then()
            modalInstance.result.then(
            //success()
            function (budget_estimate) {

                budget_estimate['csrfmiddlewaretoken'] = $cookies['csrftoken'];
                budget_estimate.budget = $scope.budget_object.resource_uri;
            // budget_estimate.budget['csrfmiddlewaretoken'] = $cookies['csrftoken'];

           $scope.budgets_promise= Budget_estimate.update(budget_estimate).$promise
            .then(function(response){
                $scope.get_data()
            })

        
        
            },//success()
            //error()
             function () {
              console.log('Modal dismissed at: ' + new Date());
            }//error()
            );//then()
   };


   //get_recurring()
   $scope.get_recurring = function(){

    var current_month = DateService.budgetdate

    var date = new Date(current_month.getFullYear(), current_month.getMonth()-1,1)

    var budgetdate = $filter('date')(date, 'yyyy-MM-dd')
    var current_budget = $scope.budget_object.id


    $scope.budgets_promise = Budget_estimate.get({extra_path:'get_recurring', date:budgetdate, current_budget:current_budget}).$promise
    .then(function(){
        $scope.get_data()

    })

    

   }//get_recurring()

     





  
}])

.controller('BudgetEstimateCtr', ['$scope','Category','$modalInstance','budget_estimates_categories', function($scope,Category,$modalInstance, budget_estimates_categories){
console.log(budget_estimates_categories)
  
    $scope.new_budget_estimate = {
        amount:0.00,
        budget_category:{},
        
    }// new_budget_estimate
    Category.get({level:0}).$promise
        .then(function(response){

            console.log(response)    
            $scope.categories = []
            $scope.added_category_names = []    

            if (budget_estimates_categories.length == 0){

                $scope.categories = response.objects
                $scope.new_budget_estimate.budget_category = $scope.categories[0]
            }//if

            else{

                angular.forEach(response.objects, function(val, indx){

                angular.forEach(budget_estimates_categories, function(category, indx2){
                    
                    $scope.added_category_names.push(category.name)
                    
                    if (val.name != category.name && $scope.added_category_names.indexOf(val.name)==-1 ){
                        console.log(val.name, category.name)
                        $scope.categories.push(val)
                        $scope.added_category_names.push(val.name)
                        return
           
                    }

                })// forEach()
            })  // forEach()  


            $scope.new_budget_estimate.budget_category = $scope.categories[0]

            }//else

        
        
        });// then()

    $scope.ok =  function(){
        $modalInstance.close($scope.new_budget_estimate)
        
    }//ok()
    $scope.cancel = function(){
        $modalInstance.dismiss()
    }//cancel()


    
}])

.controller('EditBudgetEstimateCtrl', ['$scope','budget_estimate','$modalInstance','Category','budget_estimates_categories', function($scope,budget_estimate,$modalInstance,Category, budget_estimates_categories){

    $scope.budget_estimate = budget_estimate


    Category.get({level:0}).$promise
        .then(function(response){
  
            $scope.categories = []
            $scope.added_category_names = []    


                angular.forEach(response.objects, function(val, indx){

                angular.forEach(budget_estimates_categories, function(category, indx2){
                    
                    $scope.added_category_names.push(category.name)
                    
                    if (val.name != category.name && $scope.added_category_names.indexOf(val.name)==-1 ){
                        console.log(val.name, category.name)
                        $scope.categories.push(val)
                        $scope.added_category_names.push(val.name)
                        return
           
                    }

                })// forEach()
            })  // forEach()  


            $scope.categories.push($scope.budget_estimate.budget_category)
            console.log($scope.categories)


        
        
        });// then()

    $scope.ok =  function(){
       $modalInstance.close(budget_estimate)
        
    }
    $scope.cancel = function(){
        $modalInstance.dismiss()
    }
    
}]) 