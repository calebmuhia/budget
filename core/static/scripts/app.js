'use stict'
angular.module('budgetApp', [
  'ui.router',
  'ngResource',
  'ui.bootstrap',
  'ngSanitize',
  'cgBusy',
  'ngCookies',
  'ngAnimate'
])
  .config(function ($interpolateProvider, $httpProvider, $resourceProvider, $stateProvider, $urlRouterProvider) {
    // Force angular to use square brackets for template tag
    // The alternative is using {% verbatim %}
    // $interpolateProvider.startSymbol('[[').endSymbol(']]');

    // CSRF Support
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';

    // This only works in angular 3!
    // It makes dealing with Django slashes at the end of everything easier.
    $resourceProvider.defaults.stripTrailingSlashes = false;

    // Django expects jQuery like headers
    // $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';

    // Routing

    $urlRouterProvider.otherwise('/');

    $stateProvider
    // transactions
      .state('transactions', {
        url: '/',
        views: {
          '':{
            templateUrl:'/static/partials/transactions.html',
            controller:'TransactionsCtrl'},
          'dateselector@transactions':{
            templateUrl:'/static/partials/dateselector.html',
            controller:'DateSelectorCtrl'},

          }
        }
      )

      .state('budgets', {
        url: '/bugdets',
        views: {
          '':{
            templateUrl:'/static/partials/budgets.html',
            controller:'BudgetsCtrl'},
          'dateselector@budgets':{
            templateUrl:'/static/partials/dateselectorbudgets.html',
            controller:'BudgetDateSelectorCtrl'},

          }
        }
        )

      
      
        
    });

