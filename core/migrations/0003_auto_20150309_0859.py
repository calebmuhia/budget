# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('categories', '__first__'),
        ('core', '0002_auto_20150309_0851'),
    ]

    operations = [
        migrations.AddField(
            model_name='budgetestimate',
            name='category',
            field=models.ForeignKey(related_name='estimates', default='', verbose_name='Category', to='categories.Category'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='transaction',
            name='category',
            field=models.ForeignKey(default='', verbose_name='Category', to='categories.Category'),
            preserve_default=False,
        ),
    ]
