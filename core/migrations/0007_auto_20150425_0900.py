# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0006_auto_20150425_0857'),
    ]

    operations = [
        migrations.AlterField(
            model_name='budget',
            name='start_date',
            field=models.DateTimeField(default=datetime.date.today, verbose_name='Start Date', db_index=True),
            preserve_default=True,
        ),
    ]
