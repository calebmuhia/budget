# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0004_auto_20150309_0906'),
    ]

    operations = [
        migrations.AlterField(
            model_name='budgetestimate',
            name='category',
            field=models.ForeignKey(related_name='category', verbose_name='Category', to='categories.Category'),
            preserve_default=True,
        ),
    ]
