# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Budget',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(default=datetime.datetime.now, verbose_name='Created')),
                ('updated', models.DateTimeField(default=datetime.datetime.now, verbose_name='Updated')),
                ('is_deleted', models.BooleanField(default=False, db_index=True, verbose_name='Is deleted')),
                ('name', models.CharField(max_length=255, verbose_name='Name')),
                ('slug', models.SlugField(unique=True, verbose_name='Slug')),
                ('start_date', models.DateTimeField(default=datetime.datetime.now, verbose_name='Start Date', db_index=True)),
            ],
            options={
                'verbose_name': 'Budget',
                'verbose_name_plural': 'Budgets',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='BudgetEstimate',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(default=datetime.datetime.now, verbose_name='Created')),
                ('updated', models.DateTimeField(default=datetime.datetime.now, verbose_name='Updated')),
                ('is_deleted', models.BooleanField(default=False, db_index=True, verbose_name='Is deleted')),
                ('amount', models.DecimalField(verbose_name='Amount', max_digits=11, decimal_places=2)),
                ('budget', models.ForeignKey(related_name='estimates', verbose_name='Budget', to='core.Budget')),
            ],
            options={
                'verbose_name': 'Budget estimate',
                'verbose_name_plural': 'Budget estimates',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(default=datetime.datetime.now, verbose_name='Created')),
                ('updated', models.DateTimeField(default=datetime.datetime.now, verbose_name='Updated')),
                ('is_deleted', models.BooleanField(default=False, db_index=True, verbose_name='Is deleted')),
                ('name', models.CharField(max_length=128, verbose_name='Name')),
                ('slug', models.SlugField(unique=True, verbose_name='Slug')),
            ],
            options={
                'verbose_name': 'Category',
                'verbose_name_plural': 'Categories',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Transaction',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(default=datetime.datetime.now, verbose_name='Created')),
                ('updated', models.DateTimeField(default=datetime.datetime.now, verbose_name='Updated')),
                ('is_deleted', models.BooleanField(default=False, db_index=True, verbose_name='Is deleted')),
                ('transaction_type', models.CharField(default=b'expense', max_length=32, verbose_name='Transaction type', db_index=True, choices=[(b'expense', 'Expense'), (b'income', 'Income')])),
                ('notes', models.CharField(max_length=255, verbose_name='Notes', blank=True)),
                ('amount', models.DecimalField(verbose_name='Amount', max_digits=11, decimal_places=2)),
                ('date', models.DateField(default=datetime.date.today, verbose_name='Date', db_index=True)),
                ('category', models.ForeignKey(verbose_name='Category', to='core.Category')),
            ],
            options={
                'verbose_name': 'Transaction',
                'verbose_name_plural': 'Transactions',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='budgetestimate',
            name='category',
            field=models.ForeignKey(related_name='estimates', verbose_name='Category', to='core.Category'),
            preserve_default=True,
        ),
    ]
