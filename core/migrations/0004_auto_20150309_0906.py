# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('categories', '__first__'),
        ('core', '0003_auto_20150309_0859'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='sub_category',
            field=models.ForeignKey(related_name='sub_category', verbose_name='SubCategory', blank=True, to='categories.Category', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='transaction',
            name='category',
            field=models.ForeignKey(related_name='transaction_category', verbose_name='Category', to='categories.Category'),
            preserve_default=True,
        ),
    ]
