# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('categories', '__first__'),
        ('core', '0007_auto_20150425_0900'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='budgetestimate',
            name='category',
        ),
        migrations.AddField(
            model_name='budgetestimate',
            name='budget_category',
            field=models.ForeignKey(related_name='budget_category', verbose_name='Category', blank=True, to='categories.Category', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='budgetestimate',
            name='budget',
            field=models.ForeignKey(related_name='budget', verbose_name='Budget', to='core.Budget'),
            preserve_default=True,
        ),
    ]
