# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0005_auto_20150422_0539'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='budget',
            name='name',
        ),
        migrations.RemoveField(
            model_name='budget',
            name='slug',
        ),
        migrations.AlterField(
            model_name='budgetestimate',
            name='category',
            field=models.ForeignKey(related_name='estimate_category', verbose_name='Category', to='categories.Category'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='transaction',
            name='category',
            field=models.ForeignKey(related_name='category', verbose_name='Category', to='categories.Category'),
            preserve_default=True,
        ),
    ]
