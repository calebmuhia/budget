from django.shortcuts import render, render_to_response
__author__ = 'caleb'
from django.shortcuts import render, render_to_response
from django.template.context import RequestContext
from django.views.generic import TemplateView
from django.views.generic.base import RedirectView

from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.core.urlresolvers import reverse
from django.views.decorators.http import require_GET
from django.http import HttpResponse, HttpResponseRedirect


from decimal import Decimal

from datetime import datetime

import json
import urlparse


# Create your views here.
class Index(TemplateView):
    template_name = "core/index.html"

    def get(self, request, *args, **kwargs):

        ctx = self.get_context_data(**kwargs)
        if not isinstance(ctx, RequestContext):
            ctx = RequestContext(self.request, ctx)

        return render_to_response(self.template_name, ctx)    
