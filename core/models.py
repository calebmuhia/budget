import datetime
from decimal import Decimal
from django.db import models


from django.utils.translation import ugettext_lazy as _
from categories.models import Category



class StandardMetadata(models.Model):
    """
    A basic (abstract) model for metadata.
    this will make alot of sence
    """
    created = models.DateTimeField(_('Created'), default=datetime.datetime.now)
    updated = models.DateTimeField(_('Updated'), default=datetime.datetime.now)
    is_deleted = models.BooleanField(_('Is deleted'), default=False, db_index=True)
    
    class Meta:
        abstract = True
    
    def save(self, *args, **kwargs):
        self.updated = datetime.datetime.now()
        super(StandardMetadata, self).save(*args, **kwargs)
    
    def delete(self):
        self.is_deleted = True
        self.save()


class ActiveManager(models.Manager):
    def get_queryset(self):
        return super(ActiveManager, self).get_queryset().filter(is_deleted=False)




class BudgetManager(ActiveManager):
    def most_current_for_date(self, date):
        return super(BudgetManager, self).get_queryset().filter(start_date__lte=date).latest('start_date')


class Budget(StandardMetadata):
    """
    An object representing a budget.
    Only estimates are tied to a budget object, which allows different budgets
    to be applied to the same set of transactions for comparision.
    """
    # name = models.CharField(_('Name'), max_length=255)
    # slug = models.SlugField(_('Slug'), unique=True)
    start_date = models.DateTimeField(_('Start Date'),
            default=datetime.date.today, db_index=True)

    objects = models.Manager()
    active = BudgetManager()

    def __unicode__(self):
        return self.start_date.strftime("%Y-%m-%d")

    def monthly_estimated_total(self):
        total = Decimal('0.0')
        for estimate in self.estimates.exclude(is_deleted=True):
            total += estimate.amount
        return total

    def yearly_estimated_total(self):
        return self.monthly_estimated_total() * 12

    def estimates_and_transactions(self, start_date, end_date):
        estimates_and_transactions = []
        actual_total = Decimal('0.0')

        for estimate in self.estimates.exclude(is_deleted=True):
            actual_amount = estimate.actual_amount(start_date, end_date)
            actual_total += actual_amount
            estimates_and_transactions.append({
                'estimate': estimate,
                'transactions': estimate.actual_transactions(start_date, end_date),
                'actual_amount': actual_amount,
            })
        
        return (estimates_and_transactions, actual_total)

    def actual_total(self, start_date, end_date):
        actual_total = Decimal('0.0')

        for estimate in self.estimates.exclude(is_deleted=True):
            actual_amount = estimate.actual_amount(start_date, end_date)
            actual_total += actual_amount
        
        return actual_total

    class Meta:
        verbose_name = _('Budget')
        verbose_name_plural = _('Budgets')

class BudgetEstimate(StandardMetadata):
    """
    The individual line items that make up a budget.
    Some examples include possible items like "Mortgage", "Rent", "Food", "Misc"
    and "Car Payment".
    """
    budget = models.ForeignKey(Budget, related_name='budget', verbose_name=_('Budget'))
    budget_category = models.ForeignKey(Category, related_name='budget_category', verbose_name=_('Category'), null=True, blank=True)
    amount = models.DecimalField(_('Amount'), max_digits=11, decimal_places=2)

    objects = models.Manager()
    active = ActiveManager()

    def __unicode__(self):
        return u"%s - %s" % (self.budget_category, self.amount)

    def yearly_estimated_amount(self):
        return self.amount * 12

    def actual_transactions(self, start_date, end_date):
        # Estimates should only report on expenses to prevent incomes from 
        # (incorrectly) artificially inflating totals.
        return Transaction.expenses.filter(category=self.category, date__range=(start_date, end_date)).order_by('date')

    def actual_amount(self, start_date, end_date):
        total = Decimal('0.0')
        for transaction in self.actual_transactions(start_date, end_date):
            total += transaction.amount
        return total
        
    class Meta:
        verbose_name = _('Budget estimate')
        verbose_name_plural = _('Budget estimates')


class TransactionManager(ActiveManager):
    def get_latest(self, limit=10):
        return self.get_queryset().order_by('-date', '-created')[0:limit]


class TransactionExpenseManager(TransactionManager):
    def get_queryset(self):
        return super(TransactionExpenseManager, self).get_queryset().filter(transaction_type='expense')


class TransactionIncomeManager(TransactionManager):
    def get_query_setset(self):
        return super(TransactionIncomeManager, self).get_queryset().filter(transaction_type='income')


class Transaction(StandardMetadata):
    """
    Represents incomes/expenses for the party doing the budgeting.
    
    Transactions are not tied to individual budgets because this allows
    different budgets to applied (like a filter) to a set of transactions.
    It also allows for budgets to change through time without altering the
    actual incoming/outgoing funds.
    """  



    TRANSACTION_TYPES = (
        ('expense', _('Expense')),
        ('income', _('Income')),
    )
    transaction_type = models.CharField(_('Transaction type'), max_length=32, choices=TRANSACTION_TYPES, default='expense', db_index=True)
    notes = models.CharField(_('Notes'), max_length=255, blank=True)
    category = models.ForeignKey(Category, verbose_name=_('Category'), related_name='category')
    sub_category = models.ForeignKey(Category, verbose_name=_('SubCategory'), blank=True, null=True, related_name='sub_category')
    amount = models.DecimalField(_('Amount'), max_digits=11, decimal_places=2)
    date = models.DateField(_('Date'), default=datetime.date.today, db_index=True)
    
    objects = models.Manager()
    active = ActiveManager()
    expenses = TransactionExpenseManager()
    incomes = TransactionIncomeManager()
            
    def __unicode__(self):
        return u"%s (%s) - %s" % (self.notes, self.get_transaction_type_display(), self.amount)
    
    class Meta:
        verbose_name = _('Transaction')
        verbose_name_plural = _('Transactions')