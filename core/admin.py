from django.contrib import admin
from core.models import Transaction,Budget, BudgetEstimate



class TransactionAdmin(admin.ModelAdmin):
    date_hierarchy = 'date'
    fieldsets = (
        (None, {
            'fields': ('transaction_type', 'notes', 'category','sub_category', 'amount', 'date'),
        }),
        ('Metadata', {
            'classes': ('collapse',),
            'fields': ('created', 'updated', 'is_deleted')
        })
    )
    list_display = ('__unicode__','notes', 'transaction_type', 'amount', 'date', 'is_deleted')
    list_filter = ('is_deleted',)
    search_fields = ('notes',)


admin.site.register(Transaction, TransactionAdmin)

    

class BudgetAdmin(admin.ModelAdmin):
    date_hierarchy = 'start_date'
    fieldsets = (
        (None, {
            'fields': ('start_date',),
        }),
        ('Metadata', {
            'classes': ('collapse',),
            'fields': ('created', 'updated', 'is_deleted')
        })
    )
    list_display = ('start_date', 'is_deleted')
    list_filter = ('is_deleted',)
    # prepopulated_fields = {
    #     'slug': ('name',),
    # }
    # search_fields = ('name',)


class BudgetEstimateAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': ('budget', 'budget_category', 'amount'),
        }),
        ('Metadata', {
            'classes': ('collapse',),
            'fields': ('created', 'updated', 'is_deleted')
        })
    )
    list_display = ('budget_category', 'budget', 'amount', 'is_deleted')
    list_filter = ('is_deleted',)


admin.site.register(Budget, BudgetAdmin)
admin.site.register(BudgetEstimate, BudgetEstimateAdmin)